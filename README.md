# OpenScope szótár

A magyar szabad szoftver honosítási közösség szótár alkalmazása

## Előfeltételek

* MariaDB
* Composer

* Példa, Fedora 30: `sudo dnf install composer mariadb-server`

### Adatbázis

Szükséges egy MariaDB/MySQL telepítés, valamint kell egy felhasználó:

* Felhasználónév: openscope
* Jelszó: openscope

Beállítási folyamat:

```
mysql_secure_installation
mysql -u root -p
```

majd a promptnál:

```
create database openscope;
grant all on openscope.* TO openscope@0.0.0.0 IDENTIFIED BY 'openscope';
```

Végül pedig futtatni kell az `init.sql` fájlt a **database** mappából.

### Composer

A PHP-s komponenseket elő kell készíteni a `composer` segítségével:

```
cd src
composer run
```

## Futtatás

Ezután indítható a PHP beépített webkiszolgálója:

```
php -S localhost:8080 router.php
```

A böngészőben itt lesz elérhető a projekt:

```
http://localhost:8080/api/<végpont>
```

## Éles használat

A program például `NGINX` alatt futtatható, a következő lépésekkel:

1. NGINX telepítése PHP és MariaDB támogatással
2. Az `src` mappa tartalmának átmásolása (composer fájlok nélkül) ide: `/var/www/html/glossary`
3. Gyökér beállítása:

```
server {
    root /var/www/html;
}
```

4. Front Controller beállítása:

```
location /glossary {
    try_files $uri $uri/ /glossary/router.php?$args;
}
```
5. NGINX újraindítása
6. Tesztelés: http://localhost/glossary/api/translations
