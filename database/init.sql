CREATE USER openscope IDENTIFIED BY openscope;

CREATE DATABASE openscope;

ALTER DATABASE openscope CHARACTER SET = utf8mb4 COLLATE = utf8mb4_hungarian_ci;

GRANT INSERT ON openscope.* TO openscope;

USE openscope;

CREATE TABLE IF NOT EXISTS part_of_speech (
  id INTEGER NOT NULL PRIMARY KEY,
  name VARCHAR(16) NOT NULL
);

INSERT INTO part_of_speech(id, name) VALUES(1, 'verb'); -- ige
INSERT INTO part_of_speech(id, name) VALUES(2, 'noun'); -- főnév
INSERT INTO part_of_speech(id, name) VALUES(3, 'adjective'); -- melléknév
INSERT INTO part_of_speech(id, name) VALUES(4, 'determiner'); -- determináns
INSERT INTO part_of_speech(id, name) VALUES(5, 'adverb'); -- határozószó
INSERT INTO part_of_speech(id, name) VALUES(6, 'pronoun'); -- névmás
INSERT INTO part_of_speech(id, name) VALUES(7, 'conjunction'); -- kötőszó
INSERT INTO part_of_speech(id, name) VALUES(8, 'interjection'); -- indulatszó
INSERT INTO part_of_speech(id, name) VALUES(9, 'preposition'); -- elöljárószó
INSERT INTO part_of_speech(id, name) VALUES(10, 'prefix'); -- előtag
INSERT INTO part_of_speech(id, name) VALUES(11, 'suffix'); -- utótag

CREATE TABLE IF NOT EXISTS original (
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  word VARCHAR(255) NOT NULL,
  part_of_speech INTEGER,
  FOREIGN KEY (part_of_speech) REFERENCES part_of_speech(id)
);

CREATE INDEX original_word ON original (word(10));
CREATE UNIQUE INDEX dictionary_word ON original(word, part_of_speech);

CREATE TABLE IF NOT EXISTS translation (
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  original_id INTEGER NOT NULL,
  word VARCHAR(255) NOT NULL,
  context VARCHAR(255),
  comment text,
  FOREIGN KEY (original_id) REFERENCES original(id)
);

-- SQL jegyzetek

-- =HA(NEM(ÜRES($B2));FKERES($B2;Kategóriák.$A$2:$B$12;2;1);0)
-- =HA($F2>0;ÖSSZEFŰZ("INSERT INTO original(word, part_of_speech) values('";$A2;"', ";$F2;");");ÖSSZEFŰZ("INSERT INTO original(word) values('";$A2;"');"))
-- =ÖSSZEFŰZ("INSERT INTO translation(original_id, word, context, comment) values(select ";")")


