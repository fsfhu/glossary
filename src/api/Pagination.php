<?php
namespace API;

class Pagination
{
    private const MAX_PAGE_SIZE = 100;

    public $pageSize = 10;
    public $currentPage = 1;
    public $startAt = 0;

    public $orderBy;
    public $order = 'ASC';

    public function __construct($params)
    {
        // Lapméret
        if (isset($params['pageSize']) && $params['pageSize'] > 0 && $params['pageSize'] <= MAX_PAGE_SIZE) {
            $this->pageSize = $params['pageSize'];
        }
        // Jelenlegi lap
        if (isset($params['currentPage'])) {
            $this->currentPage = $params['currentPage'];
        }
        // Kezdősor az adatbázisban
        $this->startAt = ($this->currentPage - 1) * $this->pageSize;
    }
}
