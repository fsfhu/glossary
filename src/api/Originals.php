<?php
namespace API;

require __DIR__ . '/../vendor/autoload.php';

use Database\DbConnector as DbConnector;
use Database\Original as Original;

class Originals
{
    private $original;
    private $pagination;
    private $filter;

    public function __construct(Pagination $pagination, ?string $filter)
    {
        $conn = new DbConnector();
        $this->original = new Original($conn->getConnection());
        $this->pagination = $pagination;
        $this->filter = $filter;
    }

    private function readOriginals() : void
    {
        http_response_code(200);
        header("Content-Type: application/json; charset=UTF-8");
        echo json_encode($this->original->list($this->pagination, $this->filter));
    }

    private function createOriginal() : void
    {
        $data = json_decode(file_get_contents('php://input'));
        if ($data->word) {
            $this->original->create($data->word);
            http_response_code(204);
        } else {
            http_response_code(400);
        }
    }

    private function readOriginal(int $id) : void
    {
        $original = $this->original->read($id);
        if ($original) {
            http_response_code(200);
            header("Content-Type: application/json; charset=UTF-8");
            echo json_encode($original);
        } else {
            http_response_code(400);
        }
    }

    private function updateOriginal(int $id) : void
    {
        $data = json_decode(file_get_contents('php://input'));
        if ($id && $data->word) {
            $this->original->update($id, $data->word);
            http_response_code(204);
        } else {
            http_response_code(400);
        }
    }

    private function deleteOriginal(int $id) : void
    {
        $this->original->delete($id);
        http_response_code(204);
    }

    public function dispatch(string $method, ?int $id) : void
    {
        // Cross Origin Resource Sharing
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET');
        header("Access-Control-Allow-Headers: X-Requested-With");

        if ($id == null) {
            if ($method === 'get') {
                $this->readOriginals();
            } elseif ($method === 'post') {
                $this->createOriginal();
            } else {
                http_response_code(405);
            }
        } elseif ($method === 'get') {
            $this->readOriginal($id);
        } elseif ($methed === 'put') {
            $this->updateOriginal($id);
        } elseif ($method === 'delete') {
            $this->deleteOriginal($id);
        } else {
            http_response_code(405);
        }
    }
}
