<?php
namespace API;

use Database\DbConnector as DbConnector;
use Database\Translation as Translation;

class Translations
{
    private $translation;
    private $pagination;
    private $filter;

    public function __construct(Pagination $pagination, ?string $filter)
    {
        $conn = new DbConnector();
        $this->translation = new Translation($conn->getConnection());
        $this->pagination = $pagination;
        $this->filter = $filter;
    }

    private function readTranslations() : void
    {
        http_response_code(200);
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        echo json_encode($this->translation->list($this->pagination, $this->filter));
    }

    private function createTranslation() : void
    {
        $data = json_decode(file_get_contents('php://input'));
        if ($data->original_id && $data->word && $data->context && $data->comment) {
            $this->translation->create($data->original_id, $data->word, $data->context, $data->comment);
            http_response_code(204);
        } else {
            http_response_code(400);
        }
    }

    private function readTranslation(int $id) : void
    {
        $translation = $this->translation->read($id);
        if ($translation) {
            http_response_code(200);
            header("Access-Control-Allow-Origin: *");
            header("Content-Type: application/json; charset=UTF-8");
            echo json_encode($translation);
        } else {
            http_response_code(400);
        }
    }

    private function updateTranslation(int $id) : void
    {
        $data = json_decode(file_get_contents('php://input'));
        if ($id && $data->original_id && $data->word) {
            $this->translation->update($id, $data->word, $data->context, $data->comment);
            http_response_code(204);
        } else {
            http_response_code(400);
        }
    }

    private function deleteTranslation(int $id) : void
    {
        $this->translation->delete($id);
        http_response_code(204);
    }

    public function dispatch(string $method, ?int $id) : void
    {
        if ($id == null) {
            if ($method === 'get') {
                $this->readTranslations();
            } elseif ($method === 'post') {
                $this->createTranslation();
            } else {
                http_response_code(405);
            }
        } elseif ($method === 'get') {
            $this->readTranslation($id);
        } elseif ($method === 'post') {
            $this->createTranslation($id);
        } elseif ($methed === 'put') {
            $this->updateTranslation($id);
        } elseif ($method === 'delete') {
            $this->deleteTranslation($id);
        } else {
            http_response_code(405);
        }
    }
}
