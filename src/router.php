<?php
require __DIR__ . '/vendor/autoload.php';

use API\Translations as Translations;
use API\Originals as Originals;
use API\Pagination as Pagination;

$uri = $_SERVER['REQUEST_URI'];
$method = strtolower($_SERVER['REQUEST_METHOD']);

if (preg_match('/\.(?:png|jpg|jpeg|gif|css|js)$/', $uri)) {
    return false; // serve the requested resource as-is.
} elseif (preg_match('/\/api\/originals\/?(\d*)/', $uri, $res)) {
		$id = $res[1] ? intval($res[1]) : null;
		$pagination = new Pagination($_GET);
		$filter = isset($_GET['filter']) ? $_GET['filter'] : null;
		$originals = new Originals($pagination, $filter);
		$originals->dispatch($method, $id);
} elseif (preg_match('/\/api\/translations\/?(\d*)/', $uri, $res)) {
		$id = $res[1] ? intval($res[1]) : null;
		$pagination = new Pagination($_GET);
		$filter = isset($_GET['filter']) ? $_GET['filter'] : null;
		$translations = new Translations($pagination, $filter);
		$translations->dispatch($method, $id);
} else {
    http_response_code(404);
}
