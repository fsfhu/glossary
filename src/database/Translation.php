<?php
namespace Database;

use PDO;

use API\Pagination as Pagination;

class Translation
{
    private $conn;
    private $table_name = 'TRANSLATION';

    public $id;
    public $original_id;
    public $word;
    public $context;
    public $comment;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function list(Pagination $pagination, ?string $filter) : array
    {
        $query = 'select t.id, o.id as oid, o.word as oword, t.word, ' .
                        't.context, t.comment from translation t inner join original o ' .
                        'on t.original_id = o.id';
        $params = array();
        // Szűrés
        if ($filter) {
            $query .= ' where t.word like ?';
            $params[] = '%' . $filter . '%';
        }
        // Lapozás
        $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $query .= ' limit ? offset ?';
        $params[] = $pagination->pageSize;
        $params[] = $pagination->startAt;

        $stmt = $this->conn->prepare($query);
        $stmt->execute($params);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $result = array();
            $result['id'] = intval($row['id']);
            $result['original'] = array();
            $result['original']['id'] = intval($row['oid']);
            $result['original']['word'] = $row['oword'];
            $result['word'] = $row['word'];
            $result['context'] = $row['context'];
            $result['comment'] = $row['comment'];
            $results[] = $result;
        }

        return $results;
    }

    public function create(int $original_id, string $word, string $context, string $comment) : void
    {
        $query = 'insert into translation(original_id, word, context, comment) values(?, ?, ?, ?)';
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$original_id, $word, $context, $comment]);
    }

    public function read(int $id) : ?self
    {
        $query = 'select id, original_id, word, context, comment from translation where id = ?';
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$id]);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            $this->id = intval($row['id']);
            $this->original_id = intval($row['original_id']);
            $this->word = $row['word'];
            $this->context = $row['context'];
            $this->comment = $row['comment'];
            return $this;
        }
        return null;
    }

    public function update(int $id, string $word, string $context, string $comment) : void
    {
        $query = 'update translation set word = ?, context = ?, comment = ? where id = ?';
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$word, $context, $comment, $id]);
    }

    public function delete(int $id) : void
    {
        $query = 'delete from translation where id = ?';
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$id]);
    }
}
