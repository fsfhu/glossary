<?php
namespace Database;

use PDO;

use API\Pagination as Pagination;

class Original
{
    private $conn;
    private $table_name = 'ORIGINAL';

    public $id;
    public $word;
    public $translations;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function list(Pagination $pagination, ?string $filter) : array
    {
        $query = 'select o.id as oid, o.word as oword, p.name as opart, t.id, t.word, t.context, t.comment ' .
                'from original o inner join translation t on o.id = t.original_id inner join part_of_speech p on o.part_of_speech = p.id';
        $params = array();
        // Szűrés
        if ($filter) {
            $query .= ' where o.word like ?';
            $params[] = '%' . $filter . '%';
        }
        // Rendezés
        $query .= ' order by o.word asc';
        // Lapozás
        $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $query .= ' limit ? offset ?';
        $params[] = $pagination->pageSize;
        $params[] = $pagination->startAt;

        $stmt = $this->conn->prepare($query);
        $stmt->execute($params);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $result = array();
            $result['id'] = intval($row['oid']);
            $result['word'] = $row['oword'];
            $result['partOfSpeech'] = $row['opart'];
            $result['translation'] = array();
            $result['translation']['id'] = $row['id'];
            $result['translation']['word'] = $row['word'];
            $result['translation']['context'] = $row['context'];
            $result['translation']['comment'] = $row['comment'];
            $results[] = $result;
        }

        return $results;
    }

    public function create(string $name) : void
    {
        $query = 'insert into original(word) values(?)';
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$name]);
    }

    public function read(int $id) : ?self
    {
        $query = 'select o.id as oid, o.word as oword, o.part_of_speech as opart, t.id, t.word, ' .
                't.context, t.comment from original o inner join translation t ' .
                'on o.id = t.original_id where o.id = ? order by t.word';
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$id]);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$row) {
            return null;
        }

        $this->id = $row['oid'];
        $this->word = $row['oword'];
        $this->partOfSpeech = $row['opart'];
        $this->translations = array();

        do {
            $translation = array();
            $translation['id'] = $row['id'];
            $translation['word'] = $row['word'];
            $translation['context'] = $row['context'];
            $translation['comment'] = $row['comment'];
            $this->translations[] = $translation;
        } while ($row = $stmt->fetch(PDO::FETCH_ASSOC));
        return $this;
    }

    public function update(int $id, string $name) : void
    {
        $query = 'update original set word = ? where id = ?';
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$name, $id]);
    }

    public function delete(int $id) : void
    {
        $query = 'delete from original where id = ?';
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$id]);
    }
}
